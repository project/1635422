The Code Migrate module is designed to assist developers in migrating websites from Drupal 6 to Drupal 7. It carries out a detailed analysis of the code base of the D6 site and outputs:
1. a list of modules involving a content type
2. a list of functions, methods, globals, and constants in a specified folder not found in D7 
3. the lower bound of the lines of the code to rewrite for migration
4. a list of modules on the site with their versions
5. a list of modules on the site that have been integrated into D7 core
6. a list of modules implementing D6 hooks with a note for those hooks not in D7
7. the module attempts to get D7 versions of the modules you select from the Drupal repository. It tries to select the best version available. However, for best results choose and download the most recent and stable version manually.
8. a list of D7 modules available.
This information can be used to estimate the coding effort for D6 to D7 migration for custom modules and for rewriting modules in D7.
It is different from the "coder/coder upgrade" module (drupal.org/project/coder) in that its primary purpose is for the planning stage of
a migration. It does not attempt to rewrite the code. It also provides information not found in the Coder module's outpus, such as content type 
usage in the code base, a list of available D7 modules, etc.


Install the module and set permissions the usual way. Download and unzip the module, put it into your /sites/all/modules/contrib directory.
Enable it and set permissions to 'administer code migrate'.

To use, go to the module's settings at
/admin/settings/code_migrate
and set the directory for analysis. Save the settings.  A list of checkboxes with modules to analyze will appear. Check off modules you want to analyze
and Save the settings once again. 

To analyze click the "Analyze my D6 code". You can also analyze by going to the URL at your site /code_migrate
To download D7 modules and compare with the D6 code base click "Find D7 versions of my contrib modules" or go to /code_migrate_contrib

On a large and/or slow site this may take a while.
If the module times out, consider analyzing fewer modules at a time (uncheck some modules).


